import Vue from 'vue'
import VueRouter from 'vue-router';

import App from './App.vue'
import Welcome from './pages/Welcome.vue';
import SignIn from './pages/SignIn.vue';
import NotFound from './pages/NotFound.vue';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    // This is the part in common for all the routes
    base: 'hello',
    routes: [
        { path: '/signin', component: SignIn }, // /hello/signin will use the SignIn page
        { path: '/', component: Welcome },      // /hello/ will show the Welcome page
        { path: '*', component: NotFound },     // any other routes will rendere a NotFound page
    ],
});

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
