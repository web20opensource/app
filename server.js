var express = require('express');
var path = require('path');
var cors = require('cors');
var app = express();

app.use(cors())

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type','strict-origin-when-cross-origin');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

// viewed at http://localhost:8080
app.get('/', function(req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.sendFile(path.join(__dirname + '/bootstrap/index.html'));
});

app.get('/play', function(req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.sendFile(path.join(__dirname + '/bootstrap/index.html'));
});

app.get('/welcome', function(req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.sendFile(path.join(__dirname + '/bootstrap/index.html'));
});

app.get('/hello', function(req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.sendFile(path.join(__dirname + '/bootstrap/index.html'));
});

app.get('/mfe/welcome/*', function(req, res) {
    var newPath = req.originalUrl.split('mfe/welcome/')[1];
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.redirect('http://192.168.1.68:8081/'+newPath);
    console.log('vue!!!');
  });


app.get('/mfe/music/*', function(req, res) {
    var newPath = req.originalUrl.split('mfe/music/')[1];
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.redirect('http://localhost:3000/'+newPath);
    console.log('react!!!');
  });
//app.use('/mfe/welcome', express.static(path.join(__dirname + '/welcome/public/')));

//app.use('/mfe/music', express.static(path.join(__dirname + '/music/public/')));




app.listen(8080);